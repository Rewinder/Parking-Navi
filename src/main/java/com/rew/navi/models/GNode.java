package com.rew.navi.models;

/**
 * 结点模板.
 *
 * Created by HuiWen Ren on 2016/8/14.
 */
public class GNode {

    private int x;
    private int y;

    public GNode parent;


    public GNode(){

    }

    public GNode(GNode gNode){

    }

    public GNode(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void setX(int x){
        this.x = x;
    }

    public int getX(){
        return x;
    }

    public void setY(int y){
        this.y = y;
    }

    public int getY(){
        return y;
    }

    public boolean equals(int x, int y){
        return this.x == x && this.y == y;
    }

}
