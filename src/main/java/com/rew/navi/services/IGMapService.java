package com.rew.navi.services;

import com.rew.navi.models.GMap;
import com.rew.navi.models.Result;

import java.util.List;

/**
 * Map - Service 接口.
 *
 * Created by HuiWen Ren on 2016/8/18.
 */

public interface IGMapService  {

    /**
     * 添加地图.
     *
     * @param gmap 地图
     * @return 添加结果
     */
    Result<Integer> addMap(GMap gmap);

    /**
     * 精确查询地图.
     *
     * @param keyWordOfName 地图名称（全名）
     * @return 查询唯一结果
     */
    Result<GMap> getMap(String keyWordOfName);

    /**
     * 查询地图.
     *
     * @param pageNum  页码
     * @param pageSize 每页限制数量
     * @param keyWordOfName 地图名称（关键字）
     * @return 查询结果列表
     */
    Result<List<GMap>> getMaps(Integer pageNum, Integer pageSize, String keyWordOfName);

    /**
     * 删除指定地图信息.
     *
     * @param keyWordOfName 地图名称（全名）
     * @return 删除结果
     */
    Result<Integer> deleteMap(String keyWordOfName);

    /**
     * 添加地图关键点信息.
     *
     * @param mapInfo 关键点坐标
     * @param mapSqr 邻接矩阵
     * @return 添加结果
     */
    Result<Integer> addMapInfo(String keyWordOfName, String mapInfo,
                               String url, String area, String mapSqr);

}
