package com.rew.navi.bases;

import java.io.Serializable;
import java.util.Objects;

/**
 * 基础类.
 *
 * Created by HuiWen Ren on 2016/8/15.
 */
class BaseBean implements Serializable {

    private static final long serialVersionUID = 2359249753396787775L;

    /**
     * id信息.
     */
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj){
            return true;
        }

        if (obj == null){
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        BaseBean other = (BaseBean) obj;

        return Objects.equals(id, other.id);
    }


}
