package com.rew.navi.daos;

import bases.TestMethods;
import com.rew.navi.MyApplication;
import com.rew.navi.models.GMap;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;


/**
 * Map - Dao层测试.
 *
 * Created by HuiWen Ren on 2016/8/14.
 */




@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MyApplication.class)
@WebAppConfiguration


public class GMapDaoTest {
    @Autowired
    IGMapDao mapDao;


    /**
     *
     * 测试插入数据.
     */

    @Test
    @Transactional
    public void should_return_right_when_addMap_1(){
        GMap gmap = new GMap();
        gmap.setName("map-test");
        gmap.setURL("1");
        assertEquals(1,mapDao.insertMap(gmap),0);
    }

    /**
     *
     * 查找不存在数据.
     */

    @Test
    @Transactional
    public void should_return_false_when_search_a_map_not_in_table(){
        List<GMap> gmap = new ArrayList<>();
        assertEquals(null,mapDao.selectMap("test-map"));
        assertEquals(gmap,mapDao.selectMaps("map-test"));
    }


    /**
     *
     * 无条件返回地图列表
     */

    @Test
    @Transactional
    public void should_return_all_map_list_when_research_map_without_other_require(){
        List<GMap> mapList = new ArrayList<>();
        // 第一次添加.
        GMap gmap1 = new GMap();
        gmap1.setName("map-test1");
        gmap1.setURL("1");
        mapList.add(gmap1);
        mapDao.insertMap(gmap1);
        // 第二次添加.
        GMap gmap2 = new GMap();
        gmap2.setName("map-test2");
        gmap2.setURL("2");
        mapList.add(gmap2);
        mapDao.insertMap(gmap2);
        // 第三次添加.
        GMap gmap3 = new GMap();
        gmap3.setName("map-test3");
        gmap3.setURL("3");
        mapList.add(gmap3);
        mapDao.insertMap(gmap3);

        assertEquals(false, mapDao.selectMaps("").isEmpty());

    }

    @Test
    @Transactional
    public void should_return_map_include_KeyWord_rightMap_from_maps(){
        List<GMap> mapList = new ArrayList<>(5);
        GMap gmap1 = new GMap();
        gmap1.setName("rightMap-test1");gmap1.setURL("1");
        mapList.add(gmap1);
        mapDao.insertMap(gmap1);
        // 第二次添加.
        GMap gmap2 = new GMap();
        gmap2.setName("wrongMap-test2");gmap2.setURL("2");
        //mapList.add(gmap2);
        mapDao.insertMap(gmap2);
        // 第三次添加.
        GMap gmap3 = new GMap();
        gmap3.setName("rightMap-test3"); gmap3.setURL("3");
        mapList.add(gmap3);
        mapDao.insertMap(gmap3);
        // 第四次添加.
        GMap gmap4 = new GMap();
        gmap4.setName("wrongMap-test4");gmap4.setURL("4");
        //mapList.add(gmap4);
        mapDao.insertMap(gmap4);
        // 第五次添加.
        GMap gmap5 = new GMap();
        gmap5.setName("rightMap-test5");gmap5.setURL("5");
        mapList.add(gmap5);
        mapDao.insertMap(gmap5);

        List<GMap> ansList = mapDao.selectMaps("rightMap");

        assertEquals(true,TestMethods.MapListEqualTest(ansList,mapList));
    }
}
