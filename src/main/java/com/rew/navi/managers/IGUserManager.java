package com.rew.navi.managers;

import com.rew.navi.models.GUser;

/**
 * User - Manager 接口.
 *
 * Created by HuiWen Ren on 2016/8/17.
 */

public interface IGUserManager {

    /**
     * 添加新用户.
     *
     * @param guser 用户
     * @return 添加结果
     */
    Integer addUser(GUser guser);

    /**
     * 查询用户信息.
     *
     * @param account 账号
     * @return 用户信息
     */
    GUser getUser(String account);

    /**
     * 删除指定用户.
     *
     * @param account 账号
     * @return 删除结果
     */
    Integer deleteUser(String account);

    /**
     * 更新用户信息.
     *
     * @param account .
     * @param password .
     * @param name .
     * @return .
     */
    Integer updateUser(String account, String password, String name);
}
