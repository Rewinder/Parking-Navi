#Parking-Navi

##Swagger
url: [http://localhost:8080/swagger-ui.html]()

车库导航服务器端
===============
程序接口
-------
###主程序路径：/nav/mobile

地图部分
-------
分路径：/map

>参数说明：
>>"area": "string"  非连通区域节点
>>"info": "string"  连通区域节点信息
>>"mapInfo": [null]  连通区域节点表
>>"name": "string"  名称
>>"url": "string"  链接
>>"keyWordOfName" 查询名称关键字
__中文逗号__

>添加地图
>>路径：/addMap
>>>接受参数：
>>>"area": "string"
>>>"info": "string"
>>>"mapInfo": [null]
>>>"name": "string"
>>>"url": "string"
>>返回信息：
>>>"bean": 0,
>>>"code": 1/-1,
>>>"message": "string"

>更新地图
>>路径：/addMapInfo/{keyWordOfName}/{mapInfo}/{area}/{URL}
>>接收参数：
>>>"keyWordOfName":"string"
>>>"mapInfo":"string"
>>>"area":"string"
>>>"URL":"string"
>>返回信息：
>>>"bean": 0,
>>>"code": 1/-1,
>>>"message": "string"

>查询地图
>>路径：/srtMap/{keyWordOfName}
>>接收参数：
>>>地图名称"keyWordOfName":"string
>>返回信息：
>>>"bean":
>>>>"area": "string",
>>>>"info": "string",
>>>>"mapInfo": [null],
>>>>"name": "string",
>>>>"url": "string"
>>>"code":  1/-1 ,
>>>"message": "string"

>删除地图
>>路径/deleteMap/{keyWordOfName}
>>接收参数：
>>>地图名称"keyWordOfName":"string
>>返回信息：
>>>"bean": 0,
>>>"code": 1/-1,
>>>"message": "string"

用户部分
-------
分路径：/user

>参数说明
>>"account" 账号（手机号）
>>"name" 昵称
>>"password" 密码

>一键登录/注册
>>路径：/loading  [POST]
>>接收参数：
>>GUser[
>>>"account":"string"
>>>"name":"string"
>>>"password":"string"]
>>返回信息
>>>"bean":
>>>>"account": "string",
>>>>"name": "string",
>>>>"password": "string"
>>>"code": 1/-1/-2,
>>>"message": "string"

 1 ： 已注册并成功登陆
-1 ： 未注册并成功注册
-2 ： 用户名或密码异常

>更新用户数据
>>路径：/update
>>接收参数：
>>>"account":"string"
>>>"name":"string"
>>>"password":"string"
>>返回信息
>>>"bean":
>>>>"account": "string",
>>>>"name": "string",
>>>>"password": "string"
>>>"code": 1/-1,
>>>"message": "string"


>删除用户
>>路径：/deleteUser/{account}
>>接收参数：
>>>"account":"string"
>>返回信息
>>>"bean": 0，
>>>"code": 1/-1,
>>>"message": "string"

导航部分
--------
分路径：/navi

>参数说明：
>>"keyWordOfName":地图名称
>>"stPointS":起点
>>"enPointS":终点
__英文逗号__

>导航
>>路径：{keyWordOfName}/{stPointS}/{enPointS}
>>接收参数
>>>"keyWordOfName":"string"
>>>"stPointS": "string"："x,y"
>>>"enPointS": "string"： "x,y"
>>返回信息
>>>"bean":
>>>>"parent": {},
>>>>"x": 0,
>>>>"y": 0
>>>>_多个点倒序返回_
>>>"code": 0,
>>>"message": "string"