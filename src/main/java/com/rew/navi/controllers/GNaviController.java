package com.rew.navi.controllers;

import com.rew.navi.models.GNode;
import com.rew.navi.models.Result;
import com.rew.navi.services.imp.GNaviService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 导航服务.
 *
 * Created by HuiWen Ren on 2016/8/21.
 */

@RestController
@RequestMapping("/nav/mobile/navi")
public class GNaviController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GNaviController.class);

    @Autowired
    GNaviService gNaviService;

    /**
     * 导航部分.
     *
     * @param keyWordOfName .
     * @param stPointS .
     * @param enPointS .
     * @return .
     */
    @RequestMapping(value = "/{keyWordOfName}/{stPointS}/{enPointS}",
            method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Result>
    addMapInfo(@PathVariable String keyWordOfName,
                                   @PathVariable String  stPointS,
                                   @PathVariable String  enPointS){
        LOGGER.info("receive request>>navi map:[{}],[{}],[{}] ", keyWordOfName, stPointS, enPointS);
        Result<List<GNode>> result = gNaviService.mapNavi(keyWordOfName, stPointS, enPointS);
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/askMapList_by_dis/{poi}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Result>
    askMapList_by_dis(@PathVariable String poi){
        LOGGER.info("receive request>>navi map:[{}] ", poi);
        Result<List<String>> result = gNaviService.findMap(poi);
        return ResponseEntity.ok(result);
    }

}
