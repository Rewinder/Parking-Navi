package com.rew.navi.configs;

import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * MyBitas扫描依赖.
 *
 * Created by HuiWen Ren on 2016/8/14.
 */
@Configuration
//注意，由于MapperScannerConfigurer执行的比较早，所以必须有下面的注解
@AutoConfigureAfter(MyBatisConfig.class)
public class MyBatisMapperScannerConfig {

    @Bean
    public MapperScannerConfigurer mapperScannerConfigurer() {
        MapperScannerConfigurer mapperScannerConfigurer =
                new MapperScannerConfigurer();
        mapperScannerConfigurer
                .setSqlSessionFactoryBeanName("sqlSessionFactory");
        mapperScannerConfigurer
                .setBasePackage("com.rew.navi.daos");
        return mapperScannerConfigurer;
    }
}