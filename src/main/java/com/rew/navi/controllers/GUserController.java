package com.rew.navi.controllers;

import com.rew.navi.enumerate.ResultTypeEnum;
import com.rew.navi.models.GUser;
import com.rew.navi.models.Result;
import com.rew.navi.services.IGUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * User - Controller 前端接口.
 * <p>
 * Created by HuiWen Ren on 2016/8/20.
 */
@RestController
@RequestMapping("/nav/mobile/user")
public class GUserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GUserController.class);

    @Autowired
    IGUserService userService;

    /**
     * 添加用户.
     *
     * @param guser 用户资料
     * @return 添加结果
     */


    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Result>
    addUser(@RequestBody GUser guser){
        LOGGER.info("receive request>>add user:[{}] ", guser);
        return ResponseEntity.ok(userService.addUser(guser));
    }



    /**
     * 查询用户.
     *
     * @param account 用户账号
     * @return 用户信息
     */


    @RequestMapping(value = "/getUser/{account}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Result>
    getUser(@PathVariable String account){
        LOGGER.info("receive request>>select user:[{}] ", account);
        return ResponseEntity.ok(userService.getUser(account));
    }


    /**
     * 删除用户.
     *
     * @param account 账号
     * @return 删除结果
     */

    @RequestMapping(value = "/deleteUser/{account}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Result>
    deleteUser(@PathVariable String account){
        LOGGER.info("receive request>>delete user:[{}] ", account);
        return ResponseEntity.ok(userService.deleteUser(account));
    }

    /**
     * 用户一键登陆/注册.
     *
     * @param guser 账号密码
     * @return 运行结果
     */
    @RequestMapping(value = "/loading", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Result>
    userLoad(@RequestBody GUser guser) {
        LOGGER.info("receive request>>load user:[{}] ", guser);
        Result<GUser> result = userService.getUser(guser.getAccount());
        if (result.getCode()
                .equals(ResultTypeEnum.NO_SUCH_USER.getValue())) {
            Result<Integer> addResult =  userService.addUser(guser);
            result.setCode(
                    addResult.getCode()
            );
            result.setMessage(
                    addResult.getMessage()
            );
            return ResponseEntity.ok(result);
        }
        if (guser.getPassword().equals(result.getBean().getPassword())){
            result.setCode(
                    ResultTypeEnum.SCUCCESS.getValue()
            );
            result.setMessage(
                    ResultTypeEnum.SCUCCESS.getDescription()
            );
        } else {
            result.setCode(
                    ResultTypeEnum.ACCOUNT_PWD_WRONG.getValue()
            );
            result.setMessage(
                    ResultTypeEnum.ACCOUNT_PWD_WRONG.getDescription()
            );
        }
        return ResponseEntity.ok(result);

    }

    /**
     * 用户信息更新.
     *
     * @param guser .
     * @return .
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Result>
    updateUser(@RequestBody GUser guser) {
        LOGGER.info("receive request>>update user:[{}] ", guser);
        return ResponseEntity.ok(userService.updateUser(guser.getAccount(),
                guser.getPassword(),
                guser.getName())
        );
    }

}
