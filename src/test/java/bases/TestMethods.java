package bases;

import com.rew.navi.models.GMap;

import java.util.List;

/**
 * Created by HuiWen Ren on 2016/8/17.
 */
public class TestMethods {

    public TestMethods(){

    }

    public void setMth(){

    }

    public void getMath(){

    }

    /**
     *
     *  判断列表相等.
     * @param a List<GMap>
     * @param b List<GMap>
     * @return boolean Equal_or_not
     */

    public static boolean MapListEqualTest(List<GMap> a, List<GMap> b){
        if(a.size()!=b.size()){
            return false;
        }
        for(int i=0;i<a.size();i++){
            if(!a.get(i).getName().equals(b.get(i).getName()))
                return false;
            else if(!a.get(i).getURL().equals(b.get(i).getURL())){
                return false;
            }
        }
        return true;
    }
}
