package com.rew.navi.models;

/**
 * 结果模板.
 *
 * Created by HuiWen Ren on 2016/8/15.
 * @param <T>
 */

public class Result<T> {

    public static final Integer FAILED = -1;
    public static final Integer SCUCCESS = 1;

    private Integer code;

    private String message;

    private T bean;

    public Result(){
        //MyBatis要求空构造函数
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setBean(T bean){
        this.bean = bean;
    }

    public T getBean(){
        return bean;
    }


}
