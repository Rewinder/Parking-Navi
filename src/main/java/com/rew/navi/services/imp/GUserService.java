package com.rew.navi.services.imp;

import com.rew.navi.enumerate.ResultTypeEnum;
import com.rew.navi.managers.IGUserManager;
import com.rew.navi.models.GUser;
import com.rew.navi.models.Result;
import com.rew.navi.services.IGUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

/**
 * User - Service 实现.
 *
 * Created by HuiWen Ren on 2016/8/18.
 */

@Service
public class GUserService implements IGUserService {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(GUserService.class);


    @Autowired
    IGUserManager userManager;

    /**
     * 用户注册.
     *
     * @param guser 用户
     * @return 注册结果
     */
    @Override
    public Result<Integer> addUser(GUser guser){
        Result<Integer> result = new Result<>();
        result.setCode(
                ResultTypeEnum.SCUCCESS.getValue());
        result.setMessage("用户添加成功");
        try {
            Integer code = userManager.addUser(guser);
            if (code != 1){
                result.setCode(ResultTypeEnum.FAILD.getValue());
                result.setMessage("用户添加失败");
            }
        } catch (DuplicateKeyException e) {
            LOGGER.error("名称重复异常：", e);
            result.setCode(ResultTypeEnum.DUPLICATE.getValue());
            result.setMessage(ResultTypeEnum.DUPLICATE.getDescription());
        } catch (RuntimeException e) {
            LOGGER.error("用户添加异常：", e);
            result.setCode(ResultTypeEnum.FAILD.getValue());
            result.setMessage("用户添加失败");
        }
        return result;
    }

    /**
     * 用户信息查询.
     *
     * @param account 账号
     * @return 查询结果
     * @throws RuntimeException
     */
    @Override
    public Result<GUser> getUser(String account){
        Result<GUser> result = new Result<>();
        result.setCode(ResultTypeEnum.SCUCCESS.getValue());
        result.setMessage("用户查询成功");
        try {
            result.setBean(userManager.getUser(account));
            if (
                    result.getCode()
                            .equals(Result.FAILED)
                    ){
                LOGGER.error("用户查询异常");
                result.setCode(Result.FAILED);
                result.setMessage("用户查询失败");
            }
        } catch (RuntimeException e) {
            LOGGER.error("用户查询异常：", e);
            result.setCode(Result.FAILED);
            result.setMessage("用户查询异常");
        }
        return result;
    }

    /**
     * 删除指定用户.
     *
     * @param account 账号
     * @return 删除结果
     * @throws RuntimeException
     */
    @Override
    public Result<Integer> deleteUser(String account){
        Result<Integer> result = new Result<>();
        result.setCode(ResultTypeEnum.SCUCCESS.getValue());
        result.setMessage("用户删除成功");
        try {
            result.setCode(userManager.deleteUser(account));
            if (result.getCode() != 1){
                LOGGER.error("用户删除异常");
                result.setCode(Result.FAILED);
                result.setMessage("用户删除异常");
            }
        } catch (RuntimeException e) {
            LOGGER.error("用户删除失败：", e);
            result.setCode(Result.FAILED);
            result.setMessage("用户删除失败");
        }
        return result;
    }

    /**
     * 更新用户信息.
     *
     * @param account .
     * @param password .
     * @param name .
     * @return .
     * @throws RuntimeException
     */
    @Override
    public Result<Integer> updateUser(String account, String password, String name) {
        Result<Integer> result = new Result<>();
        result.setCode(ResultTypeEnum.SCUCCESS.getValue());
        result.setMessage("数据更新成功");
        try {
            result.setCode(userManager.updateUser(account, password, name));
            if (result.getCode() != 1){
                LOGGER.error("数据更新异常");
                result.setCode(Result.FAILED);
                result.setMessage("数据更新异常");
            }
        } catch (RuntimeException e) {
            LOGGER.error("数据更新失败：", e);
            result.setCode(Result.FAILED);
            result.setMessage("数据更新失败");
        }
        return result;
    }
}
