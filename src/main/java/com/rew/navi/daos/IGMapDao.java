package com.rew.navi.daos;

import com.rew.navi.models.GMap;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 基础Dao层-数据库操作.
 *
 * Created by HuiWen Ren on 2016/8/14.
 */

@Component
public interface IGMapDao {
    Integer insertMap(GMap gmap);
    Integer deleteMap(@Param("keyWordOfName") String keyWordOfName);
    GMap selectMap(@Param("keyWordOfName") String keyWordOfName);
    List<GMap> selectMaps(@Param("keyWordOfName") String keyWordOfName);
    Integer setMapInfo(@Param("keyWordOfName") String keyWordOfName,
                       @Param("mapInfo") String mapInfo,
                       @Param("url") String URL,
                       @Param("area") String area,
                       @Param("sqr") String mapSqr);  //Important Point TEXT
    Integer addMapSqr(@Param("sqr") String mapSqr);

}
