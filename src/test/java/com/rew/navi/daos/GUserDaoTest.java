package com.rew.navi.daos;

import com.rew.navi.MyApplication;
import com.rew.navi.models.GUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;

/**
 * User - Dao层测试.
 *
 * Created by HuiWen Ren on 2016/8/20.
 */


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MyApplication.class)
@WebAppConfiguration

public class GUserDaoTest {

    @Autowired
    IGUserDao userDao;


    @Test
    @Transactional
    public void should_return_right_after_add_a_user(){
        GUser guser = new GUser();
        guser.setAccount("123");
        guser.setPassword("123");
        guser.setName("abc");
        assertEquals(1,userDao.insertUser(guser),0);
    }
}
