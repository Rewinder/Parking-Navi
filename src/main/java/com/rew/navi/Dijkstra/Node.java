package com.rew.navi.Dijkstra;

import java.util.HashMap;
import java.util.Map;

/**
 * .
 *
 * Created by HuiWen Ren on 2016/9/2.
 */
public class Node {
    private String name;
    private Map<Node, Integer> child = new HashMap<>();
    public Node(String name){
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Map<Node, Integer> getChild() {
        return child;
    }
    public void setChild(Map<Node, Integer> child) {
        this.child = child;
    }
}
