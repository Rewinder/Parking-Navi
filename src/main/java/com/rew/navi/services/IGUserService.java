package com.rew.navi.services;

import com.rew.navi.models.GUser;
import com.rew.navi.models.Result;

/**
 * User - Service 接口.
 *
 * Created by HuiWen Ren on 2016/8/18.
 */


public interface IGUserService {
    /**
     * 用户注册.
     *
     * @param guser 用户
     * @return 注册结果
     */
    Result<Integer> addUser(GUser guser);

    /**
     * 用户信息查询.
     *
     * @param account 账号
     * @return 查询结果
     */
    Result<GUser> getUser(String account);

    /**
     * 删除指定用户.
     *
     * @param account 账号
     * @return 删除结果
     */
    Result<Integer> deleteUser(String account);

    /**
     * 更新用户信息.
     *
     * @param account .
     * @param password .
     * @param name .
     * @return .
     */
    Result<Integer> updateUser(String account, String password, String name);
}
