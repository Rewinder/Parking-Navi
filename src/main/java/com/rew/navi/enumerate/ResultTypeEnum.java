package com.rew.navi.enumerate;

/**
 * 结果枚举类.
 *
 * Created by Jacob on 2016/9/4.
 */
public enum ResultTypeEnum {
    SCUCCESS(1, "成功"),
    FAILD(0, "失败"),
    FIRST_LOGIN(2, "第一次登录"),
    DUPLICATE(3, "用户名重复"),
    ACCOUNT_PWD_WRONG(4, "密码错误"),
    NO_SUCH_USER(5, "用户不存在");

    private final Integer value;
    private final String description;

    ResultTypeEnum(Integer value, String description) {
        this.value = value;
        this.description = description;
    }

    public Integer getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }


}
