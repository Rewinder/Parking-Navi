package com.rew.navi.managers.imp;

import com.github.pagehelper.PageHelper;
import com.rew.navi.daos.IGMapDao;
import com.rew.navi.managers.IGMapManager;
import com.rew.navi.models.GMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Map - Manager 实现.
 *
 * Created by HuiWen Ren on 2016/8/16.
 */

@Component
public class GMapManager implements IGMapManager {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(GMapManager.class);

    @Autowired
    IGMapDao mapDao;

    @Override
    public Integer addMap(GMap gmap) {
        return mapDao.insertMap(gmap);

    }

    @Override
    public Integer deleteMap(String keyWordOfName){
        return mapDao.deleteMap(keyWordOfName);
    }

    @Override
    public GMap getMap(String keyWordOfName){
        return mapDao.selectMap(keyWordOfName);
    }

    @Override
    public List<GMap> getMaps(Integer pageNum,
                                Integer pageSize,
                                String keyWordOfName){

        PageHelper.startPage(pageNum, pageSize, "name");

        return mapDao.selectMaps(keyWordOfName);
    }

    @Override
    public Integer addMapInfo(String keyWordOfName, String mapInfo, String url,
                              String area, String mapSqr){
        try {
            return mapDao.setMapInfo(keyWordOfName, mapInfo, url, area, mapSqr);
        } catch (RuntimeException e) {
            LOGGER.error("地图信息添加异常：", e);
            return -1;
        }
    }
}
