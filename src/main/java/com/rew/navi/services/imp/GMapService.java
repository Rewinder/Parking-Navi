package com.rew.navi.services.imp;

import com.rew.navi.enumerate.ResultTypeEnum;
import com.rew.navi.managers.imp.GMapManager;
import com.rew.navi.models.GMap;
import com.rew.navi.models.Result;
import com.rew.navi.services.IGMapService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

/**
 * Map - Service 实现.
 *
 * Created by HuiWen Ren on 2016/8/18.
 */

@Service
public class GMapService implements IGMapService {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(GMapService.class);

    @Autowired
    GMapManager mapManager;

    /**
     * 添加地图.
     *
     * @param gmap 地图
     * @return 添加结果
     */
    @Override
    public Result<Integer> addMap(GMap gmap) {
        Result<Integer> result = new Result<>();
        result.setCode(ResultTypeEnum.SCUCCESS.getValue());
        result.setMessage("地图添加成功");
        try {
            Integer code = mapManager.addMap(gmap);
            result.setBean(code);
            if (code != 1) {
                result.setCode(Result.FAILED);
                result.setMessage("添加地图" + ResultTypeEnum.FAILD);
            }
        } catch (DuplicateKeyException e) {
            LOGGER.error("名称重复异常:", e);
            result.setCode(Result.FAILED);
            result.setMessage("地图名称重复");
        } catch (RuntimeException e) {
            LOGGER.error("添加地图异常:", e);
            result.setCode(Result.FAILED);
            result.setMessage("添加地图失败");
        }
        return result;
    }

    /**
     * 模糊查询地图.
     *
     * @param pageNum  页码
     * @param pageSize 每页限制数量
     * @param keyWordOfName 地图名称（关键字）
     * @return 查询结果
     */
    @Override
    public Result<List<GMap>> getMaps(Integer pageNum, Integer pageSize, String keyWordOfName){
        Result<List<GMap>> result = new Result<>();
        result.setCode(ResultTypeEnum.SCUCCESS.getValue());
        result.setMessage("地图查询成功");
        try {
            result.setBean(
                    mapManager.getMaps(pageNum, pageSize, keyWordOfName));
            if (result.getBean().isEmpty()){
                result.setCode(Result.FAILED);
                result.setMessage("地图查询" + ResultTypeEnum.FAILD);
                LOGGER.error("地图查询失败");
            }

        } catch (RuntimeException e) {
            LOGGER.error("添加地图异常:", e);
            result.setCode(Result.FAILED);
            result.setMessage("添加地图失败");
        }
        return result;
    }

    /**
     * 精确查询地图.
     *
     * @param keyWordOfName 地图名称（全名）
     * @return 查询结果
     */
    @Override
    public Result<Integer> deleteMap(String keyWordOfName) {
        Result<Integer> result = new Result<>();
        result.setCode(ResultTypeEnum.SCUCCESS.getValue());
        result.setMessage("地图删除成功");
        try {
            result.setCode(mapManager.deleteMap(keyWordOfName));
            if (result.getCode() != 1){
                result.setCode(Result.FAILED);
                LOGGER.error("地图删除失败");
                result.setMessage("地图删除失败");
            }
        } catch (RuntimeException e) {
            LOGGER.error("地图添加异常:", e);
            result.setCode(Result.FAILED);
            result.setMessage("地图添加失败");
        }
        return result;
    }

    /**
     * 添加地图信息.
     *
     * @param keyWordOfName 名称
     * @param mapInfo 关键点坐标
     * @param url url
     * @param area 不可通过区域
     * @param mapSqr 邻接表
     * @return 添加结果
     */
    @Override
    public Result<Integer> addMapInfo(String keyWordOfName, String mapInfo,
                                      String url, String area, String mapSqr) {
        Result<Integer> result = new Result<>();
        result.setCode(ResultTypeEnum.SCUCCESS.getValue());
        result.setMessage("地图信息添加成功");
        try {
            result.setCode(mapManager.addMapInfo(keyWordOfName, mapInfo, url, area, mapSqr));
            if (result.getCode() == 0){
                GMap gMap = new GMap();
                gMap.setName(keyWordOfName);
                mapManager.addMap(gMap);
                result.setCode(mapManager.addMapInfo(keyWordOfName, mapInfo, url, area, mapSqr));
            }
        } catch (RuntimeException e) {
           result.setCode(Result.FAILED);
           result.setMessage("地图信息添加失败");
           LOGGER.error("地图信息添加失败：", e);
        }
        return result;
    }

    @Override
    public Result<GMap> getMap(String keyWordOfName) {
        Result<GMap> result = new Result<>();
        result.setCode(ResultTypeEnum.SCUCCESS.getValue());
        result.setMessage("地图查询成功");
        try {
            result.setBean(
                    mapManager.getMap(keyWordOfName));
            Assert.isTrue (result.getBean().getURL().length() > 0);

        } catch (RuntimeException e) {
            LOGGER.error("地图查询异常:", e);
            result.setCode(Result.FAILED);
            result.setMessage("地图查询失败");
        }
        return result;
    }


}
