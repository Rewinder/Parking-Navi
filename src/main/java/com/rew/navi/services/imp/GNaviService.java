package com.rew.navi.services.imp;

import com.rew.navi.Dijkstra.Dijkstra;
import com.rew.navi.enumerate.ResultTypeEnum;
import com.rew.navi.models.GMap;
import com.rew.navi.models.GNode;
import com.rew.navi.models.Result;
import com.rew.navi.services.IGNaviService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * 导航服务实现.
 *
 * Created by HuiWen Ren on 2016/9/6.
 */
@Service
public class GNaviService implements IGNaviService{
    @Autowired
    GMapService mapService;

    private static final Logger LOGGER = LoggerFactory
            .getLogger(GNaviService.class);

    @Override
    public Result<List<GNode>> mapNavi(String keyWordOfName, String stPointS, String enPointS) {
        Result<List<GNode>> result = new Result<>();

        result.setCode(
                ResultTypeEnum.SCUCCESS.getValue()
        );
        result.setMessage(
                "导航" + ResultTypeEnum.SCUCCESS.getDescription()
        );

        String[] st = stPointS.split(",");
        String[] en = enPointS.split(",");

        GNode stPoint = new GNode(Integer.valueOf(st[0]), Integer.valueOf(st[1]));
        GNode enPoint = new GNode(Integer.valueOf(en[0]), Integer.valueOf(en[1]));

        try {
            GMap gMap = mapService.getMap(keyWordOfName).getBean();
            Dijkstra dijkstra = new Dijkstra();
            result.setBean(dijkstra.runDijskra(gMap, stPoint, enPoint));

            Assert.notNull(result.getBean());

        } catch (RuntimeException e){
            LOGGER.error("导航规划错误：", e);
            result.setCode(
                    ResultTypeEnum.FAILD.getValue()
            );
            result.setMessage(
                    "导航" + ResultTypeEnum.FAILD.getDescription()
            );
        }
        return result;
    }

    @Override
    public Result<List<String>> findMap(String gNode) {
        Result<List<String>> result = new Result<>();
        List<GMap> gmapList = new ArrayList<>();
        List<String> mapNameList = new ArrayList<>();
        gmapList = mapService.getMaps(1, 20, "").getBean();
        for (GMap aGmapList : gmapList) {
            mapNameList.add(aGmapList.getName());
        }
        result.setBean(mapNameList);
        result.setCode(ResultTypeEnum.SCUCCESS.getValue());
        result.setMessage(ResultTypeEnum.SCUCCESS.getDescription());
        return result;
    }
}
