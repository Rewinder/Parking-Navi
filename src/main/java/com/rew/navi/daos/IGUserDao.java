package com.rew.navi.daos;

import com.rew.navi.models.GUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/**
 * 基础Dao层-数据库操作.
 *
 * Created by HuiWen Ren on 2016/8/17.
 */

@Component
public interface IGUserDao {
    Integer insertUser(GUser guser);
    GUser selectUser(String account);
    Integer deleteUser(String account);
    Integer updateUser(@Param("account") String account,
                       @Param("password") String password,
                       @Param("name") String name);
}
