package com.rew.navi.models;

/**
 * 地图模板.
 *
 * Created by HuiWen Ren on 2016/8/16.
 */
public class GMap {
    /**
     *
     * 地图信息部分.
     */

    // 地图名称.
    private String name;

    // 地图图片.
    private String url;

    //地图
    private int[][] mapInfo; // 邻接表
    private String info; // 可用节点信息
    private String area; // 不可用节点信息
    private String sqr; // 邻接表


    /**
     *
     * 地图功能部分.
     */
    public GMap(){
        // MyBaits必需空构造函数
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getURL() {
        return url;
    }

    public void setURL(String url) {
        this.url = url;
    }

    public int[][] getMapInfo() {
        return mapInfo;
    }

    public void setMapInfo(int[][] mapInfo) {
        this.mapInfo = mapInfo;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getSqr() {
        return sqr;
    }

    public void setSqr(String sqr) {
        this.sqr = sqr;
    }
}
