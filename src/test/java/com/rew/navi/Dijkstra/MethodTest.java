package com.rew.navi.Dijkstra;

import com.rew.navi.MyApplication;
import com.rew.navi.models.GMap;
import com.rew.navi.models.GNode;
import com.rew.navi.services.imp.GMapService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;

/**
 * DIJKSTRA test.
 *
 * Created by HuiWen Ren on 2016/9/3.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MyApplication.class)
@WebAppConfiguration
public class MethodTest {

    @Autowired
    GMapService gMapService;

    @Test
    @Transactional
    public void should_return_list_when_ask(){
        GMap gMap = new GMap();
        gMap.setURL(null);
        gMap.setSqr(null);
        gMap.setName("MapForMethodTest");
        gMap.setInfo("35，33，35，141，35，272，35，408，35，515，172，33，172，141，172，272，172，408，172，515，252，33，252，515，307，141，307，272，307，408，22，141，22，408，172，19，172，529，312，141，312，408");
        gMap.setArea("52，46，52，120，151，46，151，120，52，164，52，237，151，164，151，237，52，309，52，383，151，309，151，383，52，427，52，504，151，427，151，504，92，46，92，120，252，46，252，120，92，164，92，237，252，164，252，237，92，309，92，383，252，309，252，383，92，427，92，504，252，427，252，504");
        GNode stPoint = new GNode(35,1);
        GNode enPoint = new GNode(312,408);
        gMapService.addMap(gMap);
        Dijkstra dijkstra = new Dijkstra();
        assertEquals(false,dijkstra.runDijskra(gMap, stPoint, enPoint).isEmpty());
    }
}
