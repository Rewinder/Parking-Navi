package com.rew.navi.models;

import com.rew.navi.bases.GEncString;

/**
 * 用户模板.
 *
 * Created by HuiWen Ren on 2016/8/17.
 */
public class GUser {
    /**
     *
     * 用户信息部分.
     */

    // 地图用户
     private String name;

    // 用户账户.
    private String account;

    // 用户密码.
    private String password;

    /**
     *
     * 用户功能部分.
     */
    public GUser(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword(){
        return password;
    }

    public void setPassword(String password){
        this.password = password;
    }

    /**
     *
     * 数据加密方面.
     */

    private boolean ENC = false;

    private GEncString Encer = new GEncString();

    public void encPasserword(){
        if (!ENC){
            password = Encer.Enc(password);
            ENC = true;
        }
    }

    public String unencPasserword(){
        if (ENC){
            return Encer.unEnc(password);
        } else {
            return password;
        }
    }


}
