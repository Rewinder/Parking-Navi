package com.rew.navi.managers;


import com.rew.navi.MyApplication;
import com.rew.navi.managers.imp.GMapManager;
import com.rew.navi.models.GMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by HuiWen Ren on 2016/8/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MyApplication.class)
@WebAppConfiguration

public class GMapManagerTest {
    @Autowired
    GMapManager mapManager;

    private List<GMap> aimList = new ArrayList<>();
    // 添加15张地图.
    @Before
    public void setUp(){
        for(int i=1;i<=15;i++){
            GMap gmap = new GMap();
            gmap.setName("map-test"+String.valueOf(i));
            gmap.setURL(String.valueOf(i));
            mapManager.addMap(gmap);
            if(i>10){
                aimList.add(gmap);
            }
        }
    }

    @Test
    @Transactional
    public void should_return_right_when_add_map_1(){
        GMap gmap = new GMap();
        gmap.setName("map-test");
        gmap.setURL("1");
        assertEquals(1,mapManager.addMap(gmap),0);
    }


    /**
     *
     * 查询地图
     */

    @Test
    @Transactional
    public void should_return_11_to_15_line_when_ask_page_2_where_there_is_15_lines(){
        // 请求查看第二页（分页为10时）.
        List<GMap> ansList = mapManager.getMaps(2,10,"");
        int baseLines = mapManager.getMaps(2,10,"").size() - 5;
        assertEquals(baseLines + 5, ansList.size());
    }

}
