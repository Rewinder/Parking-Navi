package com.rew.navi.services;

import com.rew.navi.MyApplication;
import com.rew.navi.models.GMap;
import com.rew.navi.services.imp.GMapService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;

/**
 * Map - Service 测试.
 *
 * Created by HuiWen Ren on 2016/8/19.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MyApplication.class)
@WebAppConfiguration
public class GMapServiceTest {
    @Autowired
    GMapService mapService;

    @Before
    public void setUp(){
        for(int i=1;i<=15;i++){
            GMap gmap = new GMap();
            gmap.setName("map-test"+String.valueOf(i));
            gmap.setURL(String.valueOf(i));
            mapService.addMap(gmap);
        }
    }

    @Test
    @Transactional
    public void should_return_right_when_add_a_map(){
        GMap gmap = new GMap();
        gmap.setName("map-test");
        gmap.setURL("1");
        assertEquals("地图添加成功",mapService.addMap(gmap).getMessage());
    }

    @Test
    @Transactional
    public void should_return_10_lines_when_ask_for_page_1_when_limit_is_10(){
        assertEquals("地图查询成功",mapService.getMaps(1,10,"").getMessage());
        assertEquals(10,mapService.getMaps(1,10,"").getBean().size());
    }

    @Test
    @Transactional
    public void should_return_the_only_map_of_an_extra_name(){
        GMap map = new GMap();
        map.setName("map-test3");
        map.setURL("3");
        assertEquals("地图查询成功",mapService.getMaps(2,10,"").getMessage());
        assertEquals(map.getName(),mapService.getMap("map-test3").getBean().getName());
    }


    @Test
    @Transactional
    public void should_return_right_when_add_map_Sqr(){
        String mapInfo = "35，33，35，141，35，272，35，408，35，515，172，33，172，141，172，272，172，408，172，515，252，33，252，515，307，141，307，272，307，408，22，141，22，408，172，19，172，529，312，141，312，408";
        String mapArea = "52，46，52，120，151，46，151，120，52，164，52，237，151，164，151，237，52，309，52，383，151，309，151，383，52，427，52，504，151，427，151，504，92，46，92，120，252，46，252，120，92，164，92，237，252，164，252，237，92，309，92，383，252，309，252，383，92，427，92，504，252，427，252，504";
        String mapSqr = "15 0 108 239 375 482 137 -1 -1 -1 -1 217 -1 -1 -1 -1 108 0 131 267 374 -1 137 -1 -1 -1 -1 -1 272 -1 -1 239 131 0 136 243 -1 -1 137 -1 -1 -1 -1 -1 272 -1 375 267 136 0 107 -1 -1 -1 137 -1 -1 -1 -1 -1 272 482 374 243 107 0 -1 -1 -1 -1 137 -1 217 -1 -1 -1 137 -1 -1 -1 -1 0 108 239 375 482 80 -1 -1 -1 -1 -1 137 -1 -1 -1 108 0 131 267 374 -1 -1 135 -1 -1 -1 -1 137 -1 -1 239 131 0 136 243 -1 -1 -1 135 -1 -1 -1 -1 137 -1 375 267 136 0 107 -1 -1 -1 -1 135 -1 -1 -1 -1 137 482 374 243 107 0 -1 80 -1 -1 -1 217 -1 -1 -1 -1 80 -1 -1 -1 -1 0 482 -1 -1 -1 -1 -1 -1 -1 217 -1 -1 -1 -1 80 482 0 -1 -1 -1 -1 272 -1 -1 -1 -1 135 -1 -1 -1 -1 378 0 131 267 -1 -1 272 -1 -1 -1 -1 135 -1 -1 245 249 131 0 136 -1 -1 -1 272 -1 -1 -1 -1 135 -1 379 -1 267 136 0 ";
        String URL = "null";
        assertEquals(1, mapService.addMapInfo("testMap", mapInfo, URL, mapArea, mapSqr).getCode(),0);
    }
}
