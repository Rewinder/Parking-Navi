package com.rew.navi.managers.imp;

import com.rew.navi.daos.IGUserDao;
import com.rew.navi.managers.IGUserManager;
import com.rew.navi.models.GUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * User - Manager 实现.
 *
 * Created by HuiWen Ren on 2016/8/17.
 */
@Component
class GUserManager implements IGUserManager {

    @Autowired
    IGUserDao userDao;

    /**
     * 添加新用户.
     *
     * @param guser 用户
     * @return 添加结果
     */
    @Override
    public Integer addUser(GUser guser){
        guser.encPasserword();
        return userDao.insertUser(guser);
    }

    /**
     * 查询用户信息.
     *
     * @param account 账号
     * @return 用户信息
     */
    @Override
    public GUser getUser(String account) {
        return userDao.selectUser(account);
    }

    /**
     * 删除指定用户.
     *
     * @param account 账号
     * @return 删除结果
     */
    @Override
    public Integer deleteUser(String account) {
        return userDao.deleteUser(account);
    }

    @Override
    public Integer updateUser(String account, String password, String name) {
        return userDao.updateUser(account, password, name);
    }
}
