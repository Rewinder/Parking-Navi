package com.rew.navi.managers;

import com.rew.navi.MyApplication;
import com.rew.navi.models.GUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;

/**
 * Created by HuiWen Ren on 2016/8/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MyApplication.class)
@WebAppConfiguration
public class GUserManagerTest {
    @Autowired
    IGUserManager userManager;

    @Test
    @Transactional
    public void should_return_1_when_add_a_user(){
        GUser guser = new GUser();
        guser.setName("user-test");
        guser.setAccount("001");
        guser.setPassword("123");
        assertEquals(1,userManager.addUser(guser),0.0001);
    }
}
