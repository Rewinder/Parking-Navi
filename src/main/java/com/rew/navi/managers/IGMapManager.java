package com.rew.navi.managers;

import com.rew.navi.models.GMap;

import java.util.List;

/**
 * Map - Manager 接口.
 *
 * Created by HuiWen Ren on 2016/8/16.
 */


public interface IGMapManager {
    Integer addMap(GMap gmap);
    Integer deleteMap(String keyWordOfName);
    GMap getMap(String keyWordOfName);
    List<GMap> getMaps(Integer pageNum,
                       Integer pageSize,
                       String keyWordOfName);
    Integer addMapInfo(String keyWordOfName, String mapInfo,
                       String url, String area, String mapSqr);
}
