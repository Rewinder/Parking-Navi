package com.rew.navi.services;

import com.rew.navi.MyApplication;
import com.rew.navi.models.GUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;

/**
 * User - Service 测试.
 *
 * Created by HuiWen Ren on 2016/8/20.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MyApplication.class)
@WebAppConfiguration
public class GUserServiceTest {
    @Autowired
    IGUserService userService;

    @Test
    @Transactional
    public void should_return_right_when_add_a_user(){
        GUser guser = new GUser();
        guser.setAccount("123");
        guser.setPassword("123");
        guser.setName("abc");
        assertEquals("用户添加成功",userService.addUser(guser).getMessage());
    }

    @Test
    @Transactional
    public void should_return_right_when_select_a_user_in_tables(){
        GUser guser = new GUser();
        guser.setAccount("123");
        guser.setPassword("123");
        guser.setName("abc");
        userService.addUser(guser);
        assertEquals("用户查询成功",userService.getUser("123").getMessage());
    }

}
