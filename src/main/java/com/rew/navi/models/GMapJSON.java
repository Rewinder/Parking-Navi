package com.rew.navi.models;

import com.alibaba.fastjson.JSONObject;

/**
 * JSON相关处理.
 *
 * Created by HuiWen Ren on 2016/9/6.
 */
public class GMapJSON {
    public GMap readJSON(com.alibaba.fastjson.JSON infoJSON){
        JSONObject object = com.alibaba.fastjson.JSON.parseObject(infoJSON.toString());
        GMap gMap = new GMap();
        gMap.setName("ansMap");
        gMap.setInfo(object.get("info").toString());
        gMap.setArea(object.get("area").toString());
        gMap.setSqr(object.get("sqr").toString());
        gMap.setURL(object.get("url").toString());
        return gMap;
    }

    public com.alibaba.fastjson.JSON setJSON(String info, String area, String sqr, String url){
        JSONObject json = new JSONObject();
        json.put("info", info);
        json.put("area", area);
        json.put("sqr", sqr);
        json.put("url", url);
        return json;
    }
}
