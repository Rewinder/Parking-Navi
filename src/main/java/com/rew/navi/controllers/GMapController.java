package com.rew.navi.controllers;

import com.rew.navi.models.GMap;
import com.rew.navi.models.Result;
import com.rew.navi.services.IGMapService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * MapController-前端接口.
 *
 * Created by HuiWen Ren on 2016/8/19.
 */

@RestController
@RequestMapping("/nav/mobile/map")
public class GMapController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GMapController.class);

    @Autowired
    IGMapService mapService;

    /**
     * 添加地图.
     *
     * @param gmap 地图实体
     * @return 添加结果
     */
    @RequestMapping(value = "/addMap", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Result>
    addMap(@RequestBody GMap gmap){
        LOGGER.info("receive request>>add map:[{}] ", gmap);
        return ResponseEntity.ok(mapService.addMap(gmap));
    }

    /**
     * 精确查询地图.
     *
     * @param keyWordOfName 地图名称（精确）
     * @return 查询结果
     */
    @RequestMapping(value = "/getMap/{keyWordOfName}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Result>
    getMap(@PathVariable String keyWordOfName){
        LOGGER.info("receive request>>select map:[{}] ", keyWordOfName);
        return ResponseEntity.ok(mapService.getMap(keyWordOfName));
    }

    /**
     * 精确删除地图.
     *
     * @param keyWordOfName 地图名称（精确）
     * @return 删除结果
     */
    @RequestMapping(value = "/deleteMap/{keyWordOfName}", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Result>
    deleteMap(@PathVariable String keyWordOfName){
        LOGGER.info("receive request>>delete map:[{}] ", keyWordOfName);
        return ResponseEntity.ok(mapService.deleteMap(keyWordOfName));
    }

    /**
     * 添加地图关键点.
     *
     * @param mapInfo .
     * @return .
     */
    @RequestMapping(value = "/addMapInfo/{keyWordOfName}/{mapInfo}/{area}/{URL}/{mapSqr}",
            method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Result>
    addMapInfo(@PathVariable String keyWordOfName, @PathVariable String area,
                               @PathVariable String mapInfo, @PathVariable String url,
                               @PathVariable String mapSqr){
        LOGGER.info("receive request>>add mapinfo:[{}] ", keyWordOfName, mapInfo, url);
        return ResponseEntity.ok(
                mapService.addMapInfo(keyWordOfName, mapInfo, url, area, mapSqr)
        );
    }

}
