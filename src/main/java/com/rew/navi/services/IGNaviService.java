package com.rew.navi.services;

import com.rew.navi.models.GNode;
import com.rew.navi.models.Result;

import java.util.List;

/**
 * 导航服务接口.
 *
 * Created by HuiWen Ren on 2016/9/6.
 */
//@FunctionalInterface
public interface IGNaviService {
    Result<List<GNode>> mapNavi(String keyWordOfName,
                                String  stPointS,
                                String  enPointS);

    Result<List<String>> findMap(String gNode);
}
